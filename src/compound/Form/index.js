import React from 'react'
import Submit from './components/Submit/'
import Previous from './components/Previous/'
import Next from './components/Next/'
import Step from './components/Step/'
import StepList from './components/StepList/'
import ButtonList from './components/ButtonList/'


class Form extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      activeStepIndex: 0,
      totalSteps: 0,
    }
  }

  static StepList = StepList
  static ButtonList = ButtonList
  static Step = Step
  static Previous = Previous
  static Next = Next
  static Submit = Submit

  getTotalSteps = children => {
    let totalSteps = 0
    for (let child of children) {
      if (child.type.name === "StepList") {
        totalSteps = child.props.children.length - 1
      }
    }
    this.setState({
      totalSteps: totalSteps
    })
  }

  submitHandler() {
    alert('form submit');
  }

  componentDidMount() {
    this.getTotalSteps(this.props.children)
  }

  render() {
    const children = React.Children.map(this.props.children, (child, index) => {
      if (child.type.name === "StepList") {
        return React.cloneElement(child, {
          activeStepIndex: this.state.activeStepIndex,
        })
      } else if (child.type.name === "ButtonList") {
        return React.cloneElement(child, {
          activeStepIndex: this.state.activeStepIndex,
          totalSteps: this.state.totalSteps,
          handleSubmit: this.submitHandler,
          goToNextStep: () => {
            this.setState({
              activeStepIndex: this.state.activeStepIndex + 1
            })
          },
          goToPreviousStep: () => {
            this.setState({
              activeStepIndex: this.state.activeStepIndex - 1
            })
          }
        })
      } else {
        return child
      }
    })
    return <form>{children}</form>
  }
}

export default Form