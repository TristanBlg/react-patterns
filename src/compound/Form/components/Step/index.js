import React from 'react'

const Step = (props) => {
  if (props.isActive) {
    return <React.Fragment>{props.render()}</React.Fragment>
  }
  return null
}

export default Step