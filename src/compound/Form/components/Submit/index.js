import React from 'react'
import { Button } from 'antd'

const Submit = (props) => {
  if (props.isLastStep) {
    return <Button type="primary" onClick={() => props.handleSubmit()}>
      Envoyer
    </Button>
  }
  return null
}

export default Submit