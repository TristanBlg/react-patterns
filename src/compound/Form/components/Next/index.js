import React from 'react'
import { Button, Icon } from 'antd'

const Next = (props) => {
  if (props.isNextActive) {
    return <Button type="button" onClick={() => props.goToNextStep()}>
      Suivant <Icon type="right" />
    </Button>
  }
  return null
}

export default Next