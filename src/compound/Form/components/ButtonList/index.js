import React from 'react'

class ButtonList extends React.Component {
  render() {
    const { activeStepIndex, totalSteps, goToPreviousStep, goToNextStep, handleSubmit, style = "" } = this.props
    const children = React.Children.map(this.props.children, child => {
      if (child.type.name === "Previous") {
        return React.cloneElement(child, {
          isPreviousActive: activeStepIndex > 0,
          goToPreviousStep
        })
      } else if (child.type.name === "Next") {
        return React.cloneElement(child, {
          isNextActive: activeStepIndex < totalSteps,
          goToNextStep
        })
      } else if (child.type.name === "Submit") {
        return React.cloneElement(child, {
          isLastStep: activeStepIndex === totalSteps,
          handleSubmit
        })
      } else {
        return child
      }
    })
    return <div style={style}>{children}</div>
  }
}

export default ButtonList