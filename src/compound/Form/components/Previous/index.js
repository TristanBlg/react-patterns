import React from 'react'
import { Button, Icon } from 'antd'

const Previous = (props) => {
  if (props.isPreviousActive) {
    return <Button type="button" onClick={() => props.goToPreviousStep()}>
      <Icon type="left" /> Previous
    </Button>
  }
  return null
}

export default Previous