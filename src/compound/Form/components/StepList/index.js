import React from 'react'

class StepList extends React.Component {
  render() {
    const { activeStepIndex, style } = this.props
    const children = React.Children.map(this.props.children, (child, index) => {
      return React.cloneElement(child, {
        isActive: index === activeStepIndex
      })
    })
    return <div style={style}>{children}</div>
  }
}

export default StepList