import React, { Component } from 'react';

import { Card, Row, Col } from 'antd';
import 'antd/dist/antd.css';

import logo from './logo.svg'

import data from './data.json'

import Contacts from './components/Contacts'
import Mouse from './renderProps/Mouse'
import CommentListContainer from './container/CommentListContainer'
import Form from './compound/Form/'
import Input from './controlled/Input'

const firstStep = () => {
  return <React.Fragment>
    <Input placeholder="FirstStep" />
  </React.Fragment>
}
const secondStep = () => {
  return <React.Fragment>
    <Input placeholder="secondStep" />
  </React.Fragment>
}
const thirdStep = () => {
  return <React.Fragment>
    <Input placeholder="thirdStep" />
  </React.Fragment>
}

class App extends Component {
  state = {
    contacts: []
  }
  componentDidMount(){
    setTimeout(() => {
      this.setState({
        contacts: data.contacts
      })
    }, 1500);
  }
  render() {
    return (
      <div style={{ background: '#ECECEC', padding: '30px', minHeight: "100vh" }}>
        <header style={{ display: "flex", justifyContent: "center", alignItems: "center", marginBottom: "20px" }}>
          <img style={{ width: "180px", maxWidth: "100%" }} src={logo} alt="Logo"/>
        </header>
        <Row gutter={16}>
          <Col xs={24} md={12} lg={6} style={{marginBottom: "10px"}}>
            <Card title="Higher Order C. (Contacts)" bordered={false}>
              <Contacts contacts={this.state.contacts} />
            </Card>
          </Col>

          <Col xs={24} md={12} lg={6} style={{marginBottom: "10px"}}>
            <Card title="Container C. (CommentList)" bordered={false}>
              <CommentListContainer />
            </Card>
          </Col>

          <Col xs={24} md={12} lg={6} style={{marginBottom: "10px"}}>
            <Mouse>
              {mouse => (
                <Card title="Render Props (Mouse)" bordered={false}>
                  <div>
                    I'm in x:{mouse.x} y:{mouse.y}
                  </div>
                </Card>
              )}
            </Mouse>
          </Col>

          <Col xs={24} md={12} lg={6} style={{marginBottom: "10px"}}>
            <Card title="Compound C. (Form)" bordered={false}>
              <Form>
                <Form.StepList style={{ marginBottom: "10px"}}>
                  <Form.Step render={firstStep} />
                  <Form.Step render={secondStep} />
                  <Form.Step render={thirdStep} />
                </Form.StepList>
                <Form.ButtonList style={{display: "flex", justifyContent: "space-between"}}>
                  <Form.Previous />
                  <Form.Next />
                  <Form.Submit />
                </Form.ButtonList>
              </Form>
            </Card>
          </Col>
        </Row>
      </div>
    );
  }
}

export default App;
