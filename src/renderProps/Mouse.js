import React, { Component } from 'react'

class Mouse extends Component {
  state = {
    x: 0,
    y: 0,
  }

  handleMouseMove = (ev) => {
    this.setState({
      x: ev.nativeEvent.offsetX,
      y: ev.nativeEvent.offsetY
    })
  }
  
  render(){
    return <div onMouseMove={this.handleMouseMove}>
      {this.props.children(this.state)}
    </div>
  }
}

export default Mouse;