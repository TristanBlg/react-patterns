import React from 'react'

export default function CommentList({comments}) {
  return <div>
    {comments && comments.map(comment => (
      <div key={comment.id}>
        <h3>{comment.author}</h3>
        <p>{comment.content}</p>
      </div>
    ))}
  </div>
}