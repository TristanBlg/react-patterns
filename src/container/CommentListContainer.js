import React, { Component } from 'react'
import data from '../data.json'
import CommentList from './CommentList'
import {Icon} from 'antd'

class CommentListContainer extends Component {
  state = {
    comments: [],
    isLoading: true
  }

  componentDidMount(){
    setTimeout(() => {
      this.setState(prevState => ({
        comments: [...prevState.comments, ...data.comments],
        isLoading: false
      }))
    }, 3000);
  }

  render(){
    return this.state.isLoading ? <div><Icon type="loading" /> loading</div> : <CommentList comments={this.state.comments} />
  }
}

export default CommentListContainer;