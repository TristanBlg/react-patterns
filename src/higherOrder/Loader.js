import React, { Component } from 'react'
import {Icon} from 'antd'

const Loader = (propName) => (WrappedComponent) => {
  return class Loader extends Component {
    isEmpty(prop){
      return (
        prop === undefined ||
        prop === null ||
        prop === 0 ||
        (prop.hasOwnProperty('length') && prop.length === 0)  ||
        (prop.constructor === Object && Object.keys(prop).length === 0)
      )
    }
    
    render(){
      return this.isEmpty(this.props[propName]) ? <div><Icon type="loading" /> loading</div> : <WrappedComponent {...this.props} />
    }
  }
}

export default Loader