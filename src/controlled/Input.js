import React from 'react'
import { Input as AntdInput } from 'antd'

class Input extends React.Component{
  state = {
    val: ""
  }

  handleChange = (ev) => {
    this.setState({ val: ev.target.value})
  }

  render(){
    return <AntdInput size="large" onChange={this.handleChange} value={this.state.val} {...this.props} />
  }
}

export default Input