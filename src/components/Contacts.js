import React from 'react'
import PropTypes from 'prop-types'
import Loader from '../higherOrder/Loader';

class Contacts extends React.Component {
    static propTypes = {
      contacts: PropTypes.array.isRequired
    }

    render(){
      return <ul>
        {this.props.contacts.map((contact, id) => {
          return <li key={id}>{contact.author}</li>
        })}
      </ul>
    }
}
export default Loader('contacts')(Contacts);